(define the-false-value (cons "false" "boolean"))
(define empty-begin 813)
(define env.init '())
(define env.global env.init)
(define fenv.init '())
(define fenv.global fenv.init)
(define denv.init '())
(define denv.global denv.init)

(define-syntax definitial
  (syntax-rules ()
    ((definitial name)
     (begin (set! env.global
                  (cons (cons 'name 'void)
                        env.global))
            'name))
    ((definitial name value)
     (begin (set! env.global
                  (cons (cons 'name value)
                        env.global))
            'name))))

(define-syntax definitial-function
  (syntax-rules ()
    ((definitial-function names)
     (begin (set! fenv.global
                  (cons (cons 'name 'void)
                        fenv.global))
            'name))
    ((definitial-function name value)
     (begin (set! fenv.global
                  (cons (cons 'name value)
                        fenv.global))
            'name))))

(define-syntax defprimitive
  (syntax-rules ()
    ((defprimitive name value arity)
     (definitial-function name
       (lambda (values)
         (if (= arity (length values))
             (apply value values)
             (error "Incorrect arity"
                    (list 'name values))))))))

(definitial t #t)
(definitial f the-false-value)
(definitial nil '())

(definitial foo)
(definitial bar)
(definitial fib)
(definitial fact)

(defprimitive cons cons 2)
(defprimitive car car 1)
(defprimitive set-cdr! set-cdr! 2)
(defprimitive + + 2)
(defprimitive eq? eq? 2)
(defprimitive < < 2)

(define atom?
  (lambda (x)
    (and (not (pair? x)) (not (null? x)))))

(define (lookup id env)
  "Look-up the variable ID in environment ENV."
  (if (pair? env)
      (if (eq? (caar env) id)
          (cdar env)
          (lookup id (cdr env)))
      (error "No such binding" id)))

(define (f.lookup id fenv)
  "Look-up function ID in function namespace FENV."
  (if (pair? fenv)
      (if (eq? (caar fenv) id)
          (cdar fenv)
          (f.lookup id (cdr fenv)))
      (lambda (values)
        (error "No such functional binding" id))))

(define (update! id env value)
  "Updates the variable ID with value VALUE in environment ENV."
  (if (pair? env)
      (if (eq? (caar env) id)
          (begin (set-cdr! (car env) value)
                 value)
          (update! id (cdr env) value))
      (error "No such binding" id)))

(define (extend env variables values)
  "Extend environment with a list of variables and values.

ENV is the environment to extend.

VARIABLES is the list of variable symbols.

VALUES is the list of values of the previous variables."
  (cond ((pair? variables)
         (if (pair? values)
             (cons (cons (car variables) (car values))
                   (extend env (cdr variables) (cdr values)))
             (error "Too few values")))
        ((null? variables)
         (if (null? values)
             env
             (error "Too much values")))
        ((symbol? variables) (cons (cons variables values) env))))

(define (invoke fn args)
  "Invoke function FN with arguments ARGS."
  (if (procedure? fn)
      (fn args)
      (error "Not a function" fn)))

(define (f.evlis exps env fenv)
  "Evaluate list of expressions.

EXPS is the list of expressions.

ENV is the variable namespace.

FENV is the function namespace."
  (if (pair? exps)
      (cons (f.evaluate (car exps) env fenv)
            (f.evlis (cdr exps) env fenv))
      '()))

(define (f.eprogn exps env fenv)
  "Evaluate list of forms in left->right order.

EXPS is the list of expressions.

ENV is the variable namespace.

FENV is the function namespace."
  (if (pair? exps)
      (if (pair? (cdr exps))
          (begin (f.evaluate (car exps) env fenv)
                 (f.eprogn (cdr exps) env fenv))
          (f.evaluate (car exps) env fenv))
      empty-begin))

(define (f.make-function variables body env fenv)
  "Create a function.

VARIABLES is the list of input variables the function takes.

BODY is the body of the function.

ENV is the variable namespace.

FENV is the function namespace."
  (lambda (values)
    (f.eprogn body (extend env variables values) fenv)))

(define (evaluate-application fn args env fenv)
  "Evaluate an application expression.

FN is the function to evaluate.

ARGS is the list of arguments to the function.

ENV is the variable namespace.

FENV is the function namespace."
  (cond ((symbol? fn)
         ((lookup fn fenv) args))
        ((and (pair? fn) (eq? (car fn) 'lambda))
         (f.eprogn (cddr fn)
                   (extend env (cadr fn) args)
                   fenv))
        (else (error "Incorrect functional term" fn))))

(define (f.evaluate e env fenv)
  "Evaluate expression.

E is the expression to evaluate.

ENV is the variable namespace.

FENV is the function namespace."
  (if (atom? e)
      (cond ((symbol? e) (lookup e env))
            ((or (number? e) (string? e) (char? e) (boolean? e) (vector? e))
             e)
            (else (error "Cannot evaluate" e)))
      (case (car e)
        ((quote) (cadr e))
        ((if) (if (f.evaluate (cadr e) env fenv)
                  (f.evaluate (caddr e) env fenv)
                  (f.evaluate (cadddr e) env fenv)))
        ((begin) (f.eprogn (cdr e) env fenv))
        ((set!) (update! (cadr e)
                         env
                         (f.evaluate (caddr e) env fenv)))
        ((lambda) (f.make-function (cadr e) (cddr e) env fenv))
        ((function) (cond ((symbol? (cadr e))
                           (lookup (cadr e) fenv))
                          (else (error " Incorrect function" (cadr e)))))
        ((flet) (f.eprogn (cddr e)
                          env
                          (extend fenv
                                  (map car (cadr e))
                                  (map (lambda (def)
                                         (f.make-function (cadr def)
                                                          (cddr def)
                                                          env
                                                          fenv))
                                       (cadr e)))))
        ((lables) (let ((new-fenv (extend fenv
                                          (map car (cadr e))
                                          (map (lambda (def) 'void)
                                               (cadr e)))))
                    (for-each (lambda (def)
                                (update! (car def)
                                         new-fenv
                                         (f.make-function (cadr def)
                                                          (cddr def)
                                                          env
                                                          new-fenv)))
                              (cadr e))
                    (f.eprogn (cddr e) env new-fenv)))
        (else (evaluate-application (car e)
                                    (f.evlis (cdr e) env fenv)
                                    env
                                    fenv)))))

(define (lisp2-interpreter)
  "A lisp2-interpreter."
  (define (toplevel)
    "The toplevel function for the lisp2 interpreter."
    (display (f.evaluate (read) env.global fenv.global))
    (toplevel))
  (toplevel))

(define (special-extend env variables)
  "Create special extension of environment.

ENV is the environment.

VARIABLES is the list of variables to add to the extended environment."
  (append variables env))

(define (cl.lookup var env denv)
  "Lookup a variable in an environment.

VAR is the variable reference.

ENV is the variable environment.

DENV is the dynamic environment."
  (let look ((env env))
    (if (pair? env)
        (if (pair? (car env))
            (if (eq? (caar env) var)
                (cdar env)
                (look (cdr env)))
            (if (eq? (car env) var)
                ;; lookup in the current dynamic environment
                (let lookup-in-denv ((denv denv))
                  (if (pair? denv)
                      (if (eq? (caar denv) var)
                          (cdar denv)
                          (lookup-in-denv (cdr denv)))
                      ;; default to the global lexical environment
                      (lookup var env.global)))
                (look (cdr env))))
        (error "No such binding" var))))

(define (df.evlis exps env fenv denv)
  "Evaluate list of expressions.

EXPS is the list of expressions.

ENV is the variable namespace.

FENV is the function namespace.

DENV is the dynamic namespace."
  (if (pair? exps)
      (cons (df.evaluate (car exps) env fenv denv)
            (df.evlis (cdr exps) env fenv denv))
      '()))

(define (df.eprogn e* env fenv denv)
  "Evaluate list of forms in left->right order.

EXPS is the list of expressions.

ENV is the variable namespace.

FENV is the function namespace.

DENV is the dynamic namespace."
  (if (pair? e*)
      (if (pair? (cdr e*))
          (begin (df.evaluate (car e*) env fenv denv)
                 (df.eprogn (cdr e*) env fenv denv))
          (df.evaluate (car e*) env fenv denv))
      empty-begin))

(define (df.make-function variables body env fenv)
  "Create a function.

VARIABLES is the list of input variables the function takes.

BODY is the body of the function.

ENV is the variable namespace.

FENV is the function namespace."
  (lambda (values denv)
    (df.eprogn body (extend env variables values) fenv denv)))

(define (df.evaluate-application fn args env fenv denv)
  "Evaluate an application expression.

FN is the function to evaluate.

ARGS is the list of arguments to the function.

ENV is the variable namespace.

FENV is the function namespace.

DENV is the dynamic namespace."
  (cond ((symbol? fn) ((f.lookup fn fenv) args denv))
        ((and (pair? fn) (eq? (car fn) 'lambda))
         (df.eprogn (cddr fn)
                    (extend env (cadr fn) args)
                    fenv
                    denv))
        (else (error "Incorrect functional term" fn))))

(define (df.evaluate e env fenv denv)
  "Evaluate expression.

E is the expression to evaluate.

ENV is the variable namespace.

FENV is the function namespace.

DENV is the dynamic namespace."
  (if (atom? e)
      (cond ((symbol? e) (cl.lookup e env denv))
            ((or (number? e) (string? e) (char? e) (boolean? e) (vector? e))
             e)
            (else (error "Cannot evaluate" e)))
      (case (car e)
        ((quote) (cadr e))
        ((if) (if (df.evaluate (cadr e) env fenv denv)
                  (df.evaluate (caddr e) env fenv denv)
                  (df.evaluate (cadddr e) env fenv denv)))
        ((begin) (df.eprogn (cdr e) env fenv denv))
        ((set!) (update! (cadr e)
                         env
                         (df.evaluate (caddr e) env fenv denv)))
        ((function)
         (cond ((symbol? (cadr e))
                (f.lookup (cadr e) fenv))
               ((and (pair? (cadr e)) (eq? (car (cadr e)) 'lambda))
                (df.make-function
                 (cadr (cadr e)) (cddr (cadr e)) env fenv))
               (else (error "Incorrect function" (cadr e)))))
        ((dynamic) (lookup (cadr e) denv))
        ((dynamic-set!)
         (update! (cadr e)
                  denv
                  (df.evaluate (caddr e) env fenv denv)))
        ((dynamic-let)
         (df.eprogn (cddr e)
                    (special-extend env
                                    (map car (cadr e)))
                    fenv
                    (extend denv
                            (map car (cadr e))
                            (map (lambda (e)
                                   (df.evaluate e env fenv denv))
                                 (map cadr (cadr e))))))
        (else (df.evaluate-application (car e)
                                       (df.evlis (cdr e) env fenv denv)
                                       env
                                       fenv
                                       denv)))))

(define (df-interpreter)
  "A dynamic lisp2-interpreter."
  (define (toplevel)
    "The toplevel function for the lisp2 interpreter."
    (display (df.evaluate (read) env.global fenv.global denv.global))
    (toplevel))
  (toplevel))

(define (dd.invoke fn args env)
  "Invoke a function with the given arguments in a specific environment.

FN is the function to invoke.

ARGS is the list of arguments passed to the function.

ENV is the environment in which the function is invoked."
  (if (procedure? fn)
      (fn args env)
      (error "Not a function" fn)))

(definitial bind/de
  (lambda (values denv)
    (if (= 3 (length values))
        (let ((tag (car values))
              (value (cadr values))
              (thunk (caddr values)))
          (dd.invoke thunk '() (extend denv (list tag) (list value))))
        (error "Incorrect arity" 'bind/de))))

(definitial assoc/de
  (lambda (values current.denv)
    (if (= 2 (length values))
        (let ((tag (car values))
              (default (cadr values)))
          (let look ((denv current.denv))
            (if (pair? denv)
                (if (eqv? tag (caar denv))
                    (cdar denv)
                    (look (cdr denv)))
                (dd.invoke default (list tag) current.denv))))
        (error "Incorrect arity" 'assoc/de))))

(define (dd.eprogn e* env denv)
  "Evaluate list of expressions.

EXPS is the list of expressions.

ENV is the variable namespace.

DENV is the dynamic namespace."
  (if (pair? e*)
      (if (pair? (cdr e*))
          (begin (dd.evaluate (car e*) env denv)
                 (dd.eprogn (cdr e*) env denv))
          (dd.evaluate (car e*) env denv))
      empty-begin))

(define (dd.make-function variables body env)
  "Create a function.

VARIABLES is the list of input variables the function takes.

BODY is the body of the function.

ENV is the variable namespace."
  (lambda (values denv)
    (dd.eprogn body (extend env variables values) denv)))

(define (dd.evlis e* env denv)
  "Evaluate list of expressions.

EXPS is the list of expressions.

ENV is the variable namespace.

DENV is the dynamic namespace."
  (if (pair? e*)
      (if (pair? (cdr e*))
          (cons (dd.evaluate (car e*) env denv)
                (dd.evlis (cdr e*) env denv))
          (list (dd.evaluate (car e*) env denv)))
      '()))

(define (dd.evaluate e env denv)
  "Evaluate expression.

E is the expression to evaluate.

ENV is the variable namespace.

DENV is the dynamic namespace."
  (if (atom? e)
      (cond ((symbol? e) (lookup e env))
            ((or (number? e) (string? e) (char? e) (boolean? e) (vector? e))
             e)
            (else (error "cannot evaluate" e)))
      (case (car e)
        ((quote) (cadr e))
        ((if) (if (dd.evaluate (cadr e) env denv)
                  (dd.evaluate (caddr e) env denv)
                  (dd.evaluate (cadddr e) env denv)))
        ((begin) (dd.eprogn (cdr e) env denv))
        ((set!) (update! (cadr e)
                         env
                         (dd.evaluate (caddr e) env denv)))
        ((lambda) (dd.make-function (cadr e) (cddr e) env))
        (else (dd.invoke (dd.evaluate (car e) env denv)
                      (dd.evlis (cdr e) env denv)
                      denv)))))

(define (dd-interpreter)
  "A dynamic lisp1-interpreter."
  (define (toplevel)
    "The toplevel function for the lisp2 interpreter."
    (display (dd.evaluate (read) env.global denv.global))
    (toplevel))
  (toplevel))

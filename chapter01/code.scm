(define the-false-value (cons "false" "boolean"))
(define empty-begin 813)
(define env.init '())
(define env.global env.init)

(define-syntax definitial
  (syntax-rules ()
    ((definitial name)
     (begin (set! env.global (cons (cons 'name 'void) env.global))
            'name))
    ((definitial name value)
     (begin (set! env.global (cons (cons 'name value) env.global))
            'name))))

(define-syntax defprimitive
  (syntax-rules ()
    ((defprimitive name value arity)
     (definitial name
       (lambda (values)
         (if (= arity (length values))
             (apply value values)
             (error "Incorrect arity"
                    (list 'name values))))))))

(definitial t #t)
(definitial f the-false-value)
(definitial nil '())

(definitial foo)
(definitial bar)
(definitial fib)
(definitial fact)

(defprimitive cons cons 2)
(defprimitive car car 1)
(defprimitive set-cdr! set-cdr! 2)
(defprimitive + + 2)
(defprimitive eq? eq? 2)
(defprimitive < < 2)

(define atom?
  (lambda (x)
    (and (not (pair? x)) (not (null? x)))))

(define (lookup id env)
  "Look-up the variable ID in environment ENV."
  (if (pair? env)
      (if (eq? (caar env) id)
          (cdar env)
          (lookup id (cdr env)))
      (error "No such binding" id)))

(define (update! id env value)
  "Updates the variable ID with value VALUE in environment ENV."
  (if (pair? env)
      (if (eq? (caar env) id)
          (begin (set-cdr! (car env) value)
                 value)
          (update! id (cdr env) value))
      (error "No such binding" id)))

(define (extend env variables values)
  "Extend environment with a list of variables and values.

ENV is the environment to extend.

VARIABLES is the list of variable symbols.

VALUES is the list of values of the previous variables."
  (cond ((pair? variables)
         (if (pair? values)
             (cons (cons (car variables) (car values))
                   (extend env (cdr variables) (cdr values)))
             (error "Too few values")))
        ((null? variables)
         (if (null? values)
             env
             (error "Too much values")))
        ((symbol? variables) (cons (cons variables values) env))))

(define (invoke fn args)
  "Invoke function FN with arguments ARGS."
  (if (procedure? fn)
      (fn args)
      (error "Not a function" fn)))

(define (eprogn exps env)
  "Sequentially evaluate a list of forms in the given environment.

EXPS is a list of the forms to evaluate.

ENV is the environment in which to evaluate EXPS."
  (if (pair? exps)
      (if (pair? (cdr exps))
          (begin (evaluate (car exps) env)
                 (eprogn (cdr exps) env))
          (evaluate (car exps) env))
      empty-begin))

(define (make-function variables body env)
  "Create a function.

VARIABLES is the list of input variables.

BODY is the list of forms that comprise the function body.

ENV is the environment where VARIABLES are bound."
  (lambda (values)
    (eprogn body (extend env variables values))))

(define (evlis exps env)
  "Evaluate a list of forms EXPS left->right in the given environment ENV."
  (if (pair? exps)
      (let ((argument1 (evaluate (car exps) env)))
        (cons argument1 (evlis (cdr exps) env)))
      '()))

(define (evaluate e env)
  "Evaluate expression E in environment ENV."
  (if (atom? e)
      (cond ((symbol? e) (lookup e env))
            ((or (number? e) (string? e) (char? e)
                 (boolean? e) (vector? e))
             e)
            (else (error "Cannout evaluate" e)))
      (case (car e)
        ((quote) (cadr e))
        ((if) (if (not (eq? (evaluate (cadr e) env) the-false-value))
                  (evaluate (caddr e) env)
                  (evaluate (cadddr e) env)))
        ((begin) (eprogn (cdr e) env))
        ((set!) (update! (cadr e) env (evaluate (caddr e) env)))
        ((lambda) (make-function (cadr e) (cddr e) env))
        (else (invoke (evaluate (car e) env)
                      (evlis (cdr e) env))))))

(define (chapter1-scheme)
  "The interpreter for chapter1."
  (define (toplevel)
    "The top level function for the chapter 1 interpreter."
    (display (evaluate (read) env.global))
    (toplevel))
  (toplevel))

(define (d.invoke fn args env)
  "Invoke a function with the given arguments in a specific environment.

FN is the function to invoke.

ARGS is the list of arguments passed to the function.

ENV is the environment in which the function is invoked."
  (if (procedure? fn)
      (fn args env)
      (error "Not a function" fn)))

(define (d.make-function variables body def.env)
  "Create a function.

VARIABLES is the list of input variables.

BODY is the list of forms that comprise the function body.

ENV is the environment in which the function is defined."
  (lambda (values current.env)
    (eprogn body (extend current.env variables values))))

(define (d.make-closure fn env)
  "Creates a closure for a function in the given environment.

FN is the function for which to create a closure.

ENV is the environment in which the function is defined."
  (lambda (values current.env)
    (fn values env)))

(define (d.evaluate e env)
  "Evaluate an expression while keeping the environment up to date with the
current lexical scope.

E is the expression to evaluate.

ENV is the top-level environment for the expression."
  (if (atom? e)
      (cond ((symbol? e) (lookup e env))
            ((or (number? e) (string? e) (char? e)
                 (boolean? e) (vector? e))
             e)
            (else (error "Cannot evaluate" e)))
      (case (car e)
        ((quote) (cadr e))
        ((if) (if (not (eq? (d.evaluate (cadr e) env) the-false-value))
                  (d.evaluate (caddr e) env)
                  (d.evaluate (cadddr e) env)))
        ((begin) (eprogn (cdr e) env))
        ((set!) (update! (cadr e) env (d.evaluate (caddr e) env)))
        ((function)
         (let* ((f (cadr e))
                (fun (d.make-function (cadr f) (cddr f) env)))
           (d.make-closure fun env)))
        ((lambda) (d.make-function (cadr e) (cddr e) env))
        (else (d.invoke (d.evaluate (car e) env)
                        (evlis (cdr e) env)
                        env)))))

(define (s.make-function variables body env)
  "Make a function."
  (lambda (values current.env)
    (let ((old-bindings
           (map (lambda (var val)
                  (let ((old-value (getprop var 'apval)))
                    (putprop var 'apval val)
                    (cons var old-value)))
                variables
                values)))
      (let ((result (eprogn body current.env)))
        (for-each (lambda (b) (putprop (car b) 'apval (cdr b)))
                  old-bindings)
        result))))

(define (s.lookup id env)
  ""
  (getprop id 'apval))

(define (s.update! id env value)
  ""
  (putprop id 'apval value))

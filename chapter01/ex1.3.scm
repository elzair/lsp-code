(define (ex1.3.lookup id env)
  "Look-up the variable ID in environment ENV."
  (if (pair? env)
      (if (eq? (caaar env) id)
          (cadar env)
          (ex1.3.lookup id (cdr env)))
      (error "No such binding" id)))

(define (ex1.3.update! id env value)
  "Updates the variable ID with value VALUE in environment ENV."
  (if (pair? env)
      (if (eq? (caaar env) id)
          (begin (set-cdr! (car env) (cons value '()))
                 value)
          (ex1.3.update! id (cdr env) value))
      (error "No such binding" id)))

; Test Code
(load "code.scm")

(define (ex1.3.extend env names values)
  "Extend environment ENV with variables in NAMES and VALUES."
  (cons (cons names values) env))

(define (ex1.3.eprogn exps env)
  "Sequentially ex1.3.evaluate a list of forms in the given environment.

EXPS is a list of the forms to ex1.3.evaluate.

ENV is the environment in which to ex1.3.evaluate EXPS."
  (if (pair? exps)
      (if (pair? (cdr exps))
          (begin (ex1.3.evaluate (car exps) env)
                 (ex1.3.eprogn (cdr exps) env))
          (ex1.3.evaluate (car exps) env))
      empty-begin))

(define (ex1.3.make-function variables body env)
  "Create a function.

VARIABLES is the list of input variables.

BODY is the list of forms that comprise the function body.

ENV is the environment where VARIABLES are bound."
  (lambda (values)
    (ex1.3.eprogn body (extend env variables values))))

(define (evlis exps env)
  "Evaluate a list of forms EXPS left->right in the given environment ENV."
  (if (pair? exps)
      (let ((argument1 (ex1.3.evaluate (car exps) env)))
        (cons argument1 (evlis (cdr exps) env)))
      '()))

(define (ex1.3.evaluate e env)
  "Evaluate expression E in environment ENV."
  (if (atom? e)
      (cond ((symbol? e) (lookup e env))
            ((or (number? e) (string? e) (char? e)
                 (boolean? e) (vector? e))
             e)
            (else (error "Cannout ex1.3.evaluate" e)))
      (case (car e)
        ((quote) (cadr e))
        ((if) (if (not (eq? (ex1.3.evaluate (cadr e) env) the-false-value))
                  (ex1.3.evaluate (caddr e) env)
                  (ex1.3.evaluate (cadddr e) env)))
        ((begin) (ex1.3.eprogn (cdr e) env))
        ((set!) (update! (cadr e) env (ex1.3.evaluate (caddr e) env)))
        ((lambda) (ex1.3.make-function (cadr e) (cddr e) env))
        (else (invoke (ex1.3.evaluate (car e) env)
                      (evlis (cdr e) env))))))

(define (ex1.3.scheme)
  "The interpreter for ex1.3."
  (define (toplevel)
    "The top level function for the interpreter."
    (display (ex1.3.evaluate (read) env.global))
    (toplevel))
  (toplevel))

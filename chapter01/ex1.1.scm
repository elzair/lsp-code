(define (ex1.1.evaluate e env)
  "Evaluate expression E in environment ENV with tracing."
  (if (atom? e)
      (cond ((symbol? e) (lookup e env))
            ((or (number? e) (string? e) (char? e)
                 (boolean? e) (vector? e))
             e)
            (else (error "Cannout evaluate" e)))
      (begin
        (display (cdr e))
        (let ((result
               (case (car e)
                 ((quote) (cadr e))
                 ((if) (if (not (eq? (ex1.1.evaluate (cadr e) env)
                                     the-false-value))
                           (ex1.1.evaluate (caddr e) env)
                           (ex1.1.evaluate (cadddr e) env)))
                 ((begin) (ex1.1.eprogn (cdr e) env))
                 ((set!) (update! (cadr e)
                                  env
                                  (ex1.1.evaluate (caddr e) env)))
                 ((lambda) (ex1.1.make-function (cadr e) (cddr e) env))
                 (else (invoke (ex1.1.evaluate (car e) env)
                               (ex1.1.evlis (cdr e) env))))))
          (display result)
          result))))

; Test Code
(load "code.scm")

(define (ex1.1.eprogn exps env)
  "Sequentially evaluate a list of forms in the given environment.

EXPS is a list of the forms to evaluate.

ENV is the environment in which to evaluate EXPS."
  (if (pair? exps)
      (if (pair? (cdr exps))
          (begin (ex1.1.evaluate (car exps) env)
                 (ex1.1.eprogn (cdr exps) env))
          (ex1.1.evaluate (car exps) env))
      empty-begin))

(define (ex1.1.make-function variables body env)
  "Create a function.

VARIABLES is the list of input variables.

BODY is the list of forms that comprise the function body.

ENV is the environment where VARIABLES are bound."
  (lambda (values)
    (ex1.1.eprogn body (extend env variables values))))

(define (ex1.1.evlis exps env)
  "Evaluate a list of forms EXPS left->right in the given environment ENV."
  (if (pair? exps)
      (let ((argument1 (ex1.1.evaluate (car exps) env)))
        (cons argument1 (ex1.1.evlis (cdr exps) env)))
      '()))

(define (ex1.1.scheme)
  "The interpreter for exercise 1.1."
  (define (toplevel)
    "The top level function for the interpreter."
    (display (ex1.1.evaluate (read) env.global))
    (toplevel))
  (toplevel))

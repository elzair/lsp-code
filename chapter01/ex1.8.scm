(load "code.scm")

(define (ex1.8.apply args)
  "Apply a function to the given arguments ARGS."
  (invoke (car args) (cadr args)))

(defprimitive apply apply 2)
(update! 'apply env.global ex1.8.apply)

; Test Code

(define (ex1.8.evlis expressions environment)
  "Iteratively evaluate a list of forms EXPS left->right in the given environment ENV."
  (define (evlis-helper store exps env)
    "Helper function for evlis."
    (if (pair? exps)
        (evlis-helper (let ((arg1 (ex1.8.evaluate (car exps) env)))
                        (if (eq? store '())
                            arg1
                            (cons store (cons arg1 '()))))
                      (cdr exps)
                      env)
        store))
  (evlis-helper '() expressions environment))

(define (ex1.8.make-function variables body env)
  "Create a function.

VARIABLES is the list of input variables.

BODY is the list of forms that comprise the function body.

ENV is the environment where VARIABLES are bound."
  (lambda (values)
    (ex1.8.eprogn body (extend env variables values))))

(define (ex1.8.eprogn exps env)
  "Sequentially evaluate a list of forms in the given environment.

EXPS is a list of the forms to evaluate.

ENV is the environment in which to evaluate EXPS."
  (if (pair? exps)
      (if (pair? (cdr exps))
          (begin (ex1.8.evaluate (car exps) env)
                 (ex1.8.eprogn (cdr exps) env))
          (ex1.8.evaluate (car exps) env))
      empty-begin))

(define (ex1.8.lt args)
  "Determine if value X is less than value Y in ARGS."
  (if (eq? (length args) 2)
      (if (< (car args) (cadr args))
          't
          'the-false-value)
      (error "Incorrect length" args)))

(update! '< env.global ex1.8.lt)

(define (ex1.8.list exps)
  "Create a list from the input expressions EXPS."
  (if (pair? exps)
      (cons (car exps)
            (ex1.8.list (cdr exps)))
      '()))

(defprimitive list list 2)
(update! 'list env.global ex1.8.list)

(define (ex1.8.evaluate e env)
  "Evaluate expression E in environment ENV with tracing, iterative evlis, a native <, a list function, and an apply function."
  (if (atom? e)
      (cond ((symbol? e) (lookup e env))
            ((or (number? e) (string? e) (char? e)
                 (boolean? e) (vector? e))
             e)
            (else (error "Cannout evaluate" e)))
      (begin
        (display (cdr e))
        (let ((result
               (case (car e)
                 ((quote) (cadr e))
                 ((if) (if (not (eq? (ex1.8.evaluate (cadr e) env)
                                     the-false-value))
                           (ex1.8.evaluate (caddr e) env)
                           (ex1.8.evaluate (cadddr e) env)))
                 ((begin) (ex1.8.eprogn (cdr e) env))
                 ((set!) (update! (cadr e)
                                  env
                                  (ex1.8.evaluate (caddr e) env)))
                 ((lambda) (ex1.8.make-function (cadr e) (cddr e) env))
                 (else (invoke (ex1.8.evaluate (car e) env)
                               (ex1.8.evlis (cdr e) env))))))
          (display result)
          result))))

(define (ex1.8.scheme)
  "The interpreter for exercise 1.8."
  (define (toplevel)
    "The top level function for the interpreter."
    (display (ex1.8.evaluate (read) env.global))
    (toplevel))
  (toplevel))

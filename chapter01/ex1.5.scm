(load "code.scm")

(define (ex1.5.lt args)
  "Determine if value X is less than value Y in ARGS."
  (if (eq? (length args) 2)
      (if (< (car args) (cadr args))
          't
          'the-false-value)
      (error "Incorrect length" args)))

(update! '< env.global ex1.5.lt)

; Test Code

(define (ex1.5.evlis expressions environment)
  "Iteratively evaluate a list of forms EXPS left->right in the given environment ENV."
  (define (evlis-helper store exps env)
    "Helper function for evlis."
    (if (pair? exps)
        (evlis-helper (let ((arg1 (ex1.5.evaluate (car exps) env)))
                        (if (eq? store '())
                            arg1
                            (cons store (cons arg1 '()))))
                      (cdr exps)
                      env)
        store))
  (evlis-helper '() expressions environment))

(define (ex1.5.make-function variables body env)
  "Create a function.

VARIABLES is the list of input variables.

BODY is the list of forms that comprise the function body.

ENV is the environment where VARIABLES are bound."
  (lambda (values)
    (ex1.5.eprogn body (extend env variables values))))

(define (ex1.5.eprogn exps env)
  "Sequentially evaluate a list of forms in the given environment.

EXPS is a list of the forms to evaluate.

ENV is the environment in which to evaluate EXPS."
  (if (pair? exps)
      (if (pair? (cdr exps))
          (begin (ex1.5.evaluate (car exps) env)
                 (ex1.5.eprogn (cdr exps) env))
          (ex1.5.evaluate (car exps) env))
      empty-begin))

(define (ex1.5.evaluate e env)
  "Evaluate expression E in environment ENV with tracing, iterative evlis and a native <."
  (if (atom? e)
      (cond ((symbol? e) (lookup e env))
            ((or (number? e) (string? e) (char? e)
                 (boolean? e) (vector? e))
             e)
            (else (error "Cannout evaluate" e)))
      (begin
        (display (cdr e))
        (let ((result
               (case (car e)
                 ((quote) (cadr e))
                 ((if) (if (not (eq? (ex1.5.evaluate (cadr e) env)
                                     the-false-value))
                           (ex1.5.evaluate (caddr e) env)
                           (ex1.5.evaluate (cadddr e) env)))
                 ((begin) (ex1.5.eprogn (cdr e) env))
                 ((set!) (update! (cadr e)
                                  env
                                  (ex1.5.evaluate (caddr e) env)))
                 ((lambda) (ex1.5.make-function (cadr e) (cddr e) env))
                 (else (invoke (ex1.5.evaluate (car e) env)
                               (ex1.5.evlis (cdr e) env))))))
          (display result)
          result))))

(define (ex1.5.scheme)
  "The interpreter for exercise 1.5."
  (define (toplevel)
    "The top level function for the interpreter."
    (display (ex1.5.evaluate (read) env.global))
    (toplevel))
  (toplevel))
